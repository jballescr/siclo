import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.sass']
})
export class PaginationComponent implements OnInit {
  // datos de entrada
  @Input() nextValue?: string;
  @Input() prevValue?: string;
  @Input() totalPageValue: number;
  // eventos
  @Output() next: EventEmitter<any>;
  @Output() prev: EventEmitter<any>;
  @Output() selectPage: EventEmitter<any>;

  currentPage = 1;
  arrNumberPages = [];

  constructor() {
    this.next = new EventEmitter();
    this.prev = new EventEmitter();
    this.selectPage = new EventEmitter();
  }

  ngOnInit(): void {
    this.arrNumberPages = new Array(this.totalPageValue);
    console.log(this.arrNumberPages);
  }
  onNext() {
    if (this.currentPage < this.totalPageValue) {
      this.currentPage++;
    }
    this.next.emit(this.currentPage);
  }
  onPrev() {
    if (this.currentPage > 1) {
      this.currentPage--;
    }
    this.next.emit(this.currentPage);
  }

  onSelectPage(page: number) {
    this.currentPage = page;
    this.selectPage.emit(page);
  }
}
