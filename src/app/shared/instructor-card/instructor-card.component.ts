import { Component, Input, OnInit } from '@angular/core';
import { Instructor } from '../../interfaces/instructor.interface';

@Component({
  selector: 'app-instructor-card',
  templateUrl: './instructor-card.component.html',
  styleUrls: ['./instructor-card.component.sass']
})
export class InstructorCardComponent implements OnInit {
  @Input() instructor: Instructor;
  constructor() { }

  ngOnInit(): void {
  }

}
