import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstructorCardComponent } from './instructor-card/instructor-card.component';
import { PaginationComponent } from './pagination/pagination.component';
import { FooterComponent } from './footer/footer.component';
import { SectionImagesComponent } from './section-images/section-images.component';



@NgModule({
  declarations: [
    InstructorCardComponent,
    PaginationComponent,
    FooterComponent,
    SectionImagesComponent
  ],
  exports: [
    InstructorCardComponent,
    PaginationComponent,
    FooterComponent,
    SectionImagesComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
