import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InstructorsRoutingModule } from './instructors-routing.module';
import { InstructorListComponent } from './instructor-list/instructor-list.component';
import { InstructorComponent } from './instructor/instructor.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    InstructorListComponent,
    InstructorComponent
  ],
  imports: [
    CommonModule,
    InstructorsRoutingModule,
    SharedModule
  ]
})
export class InstructorsModule { }
