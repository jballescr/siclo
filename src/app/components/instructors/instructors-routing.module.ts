import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InstructorComponent } from './instructor/instructor.component';
import { InstructorListComponent } from './instructor-list/instructor-list.component';


const routes: Routes = [
  {
    path: '',
    component: InstructorListComponent
  },
  {
    path: 'instructor',
    component: InstructorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstructorsRoutingModule { }
