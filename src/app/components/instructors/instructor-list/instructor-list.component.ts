import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Response, Instructor, Result } from '../../../interfaces';

@Component({
  selector: 'app-instructor-list',
  templateUrl: './instructor-list.component.html',
  styleUrls: ['./instructor-list.component.sass']
})
export class InstructorListComponent implements OnInit {
  response: Response = null;
  instructors: Array<Instructor> = [];
  data: Result = null;
  pages = 1;


  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.loadData(10);
  }

  loadData(size: number, page: number = 0) {
    this.apiService.getData( size, page ).subscribe(
      (res) => {
        this.response = res;
      },
      error => {

      },
      () => {
        if ( this.response.code === 200 ) {
          this.data = this.response.data;
          if ( this.data ) {
            this.pages = this.data.total_pages;
            this.instructors = this.data.results;
          }
        }
      }
    );
  }

}
