import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Response } from '../interfaces';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private api = 'https://api.siclo.com/api/v2/plus/instructors/?format=json';
  constructor( private http: HttpClient) { }

  getData(size: number, page: number = null): Observable<Response>{
    const SET_PAGE = page ? `&page=${ page }` : '';
    return this.http.get(`${ this.api }&page_size=${ size }${ SET_PAGE }`).pipe(
      map(( response: Response ) => {
        return response as Response;
      })
    );
  }
}
