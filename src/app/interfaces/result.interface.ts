import { Instructor } from '.';

export interface Result {
    count: number;
    total_pages: number;
    results: Array<Instructor>;
    links: {
        previous: string;
        next: string;
    };
}
