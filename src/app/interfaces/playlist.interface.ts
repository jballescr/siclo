export interface Playlist {
    id: number;
    name: string;
    reference_id: string;
    active: string;
}
