export * from './instructor.interface';
export * from './interests.interface';
export * from './result.interface';
export * from './playlist.interface';
export * from './response.interface';