export interface Interests {
    instructor: number;
    philosophy: string;
    fitness: string;
    music: string;
    activities: string;
    surprise: string;
    inspiration: string;
}
