import { Interests } from './interests.interface';
import { Playlist } from './playlist.interface';
export interface Instructor {
    id: string;
    nombre: string;
    interests: Interests;
    full_photo: string;
    face_photo: string;
    playlist: Playlist;
}
