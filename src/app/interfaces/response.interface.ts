import { Result } from './result.interface';
export interface Response {
    message: boolean;
    code: number;
    data: Result;
}
